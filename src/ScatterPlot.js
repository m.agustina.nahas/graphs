import React from "react";
import * as d3 from "d3";
import './ScatterPlot.css';
import { Scrollama, Step } from 'react-scrollama';

const ScatterPlot = ({ data }) => {

    var margin = {top: 100, right: 200, bottom: 50, left: 125, tikX: 15, tikY: 20};
    const width = 960,
        height=800;

    const [isMobile, setMobile] = React.useState(false)
    const [step, setStep] = React.useState(0)

    const [rankData, setRankData] = React.useState([])

    const ref = React.useRef(null);

    const handleResize = () => {
        setMobile(window.innerWidth < 960);
    }

    const onStepEnter = ({ data }) => {
        setStep(data);
        d3.select("svg").attr("class", "step_" + (data));
    };

    React.useEffect(() => {
        handleResize();

        window.addEventListener("resize", handleResize);
        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    // Add X axis
    var x = d3.scaleLinear()
        .domain([-0.6, 0.6])
        .range([ 0, width ]);

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([0, 1])
        .range([ height, 0]);

    const drawEverything = React.useCallback(() => {
        d3.select(ref.current).select("svg").remove();

        const svg = d3
            .select(ref.current)
            .append("svg")
            .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var yAxis = d3.axisLeft(y)
            .tickValues([0, 0.2, 0.4, 0.6, 0.8, 1])
            .tickSize(10);

        var xAxis = d3.axisBottom(y)
            .tickValues([-0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6])
            .tickSize(10);

        var xGroup = svg.append("g");
        var xAxisElem = xGroup.append("g")
            .attr("class", "x-axis")
            .attr("transform", "translate(" + [-width/3, height] + ")")
            .call(xAxis);

        xGroup.append("g").selectAll("line")
            .data(x.ticks(5))
            .enter().append("line")
            .attr("class", "grid-line")
            .attr("y1", 0)
            .attr("y2", height)
            .attr("x1", d => x(d))
            .attr("x2", d => x(d));

        var yGroup = svg.append("g");
        var yAxisElem = yGroup.append("g")
            .attr("transform", "translate(" + [0, 0] + ")")
            .attr("class", "y-axis")
            .call(yAxis);

        yGroup.append("g").selectAll("line")
            .data([0])
            .enter().append("line")
            .attr("class", "grid-line")
            .attr("y1", d => y(d))
            .attr("y2", d => y(d))
            .attr("x1", 0)
            .attr("x2", width);


    }, [rankData, isMobile])

    const getClasses = (d) => {
        return "scatter_plot_point" + (d.RCA_ARG > 0.165 ? " step_1" : "") + (d.COG_ARG > 0.2 ? " step_2" : "")
    }

    React.useEffect(() => {
        d3.csv("./green_prod_arg.csv").then((objList) => {
            const data = objList.filter((obj) => {return obj.RCA_ARG < 1 && obj.RCA_ARG > 0 })

            const svg = d3.select(ref.current).select("svg");

            const d3gs = svg.append("g")
                .selectAll("path")
                .data(data)
                .enter().append("g")
                .attr("class", "scatter_plot_g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            const points = d3gs.append("circle")
                .attr("cx", function (d) { return x(d.COG_ARG); } )
                .attr("cy", function (d) { return y(d.RCA_ARG); } )
                .attr("class", d =>  getClasses(d))
                .attr("r", 4)
                .attr("stroke", d => d.color)
                .attr("fill", d => d.color)
        })
    },[])

    React.useEffect(() => {
        drawEverything();
    }, [drawEverything])

    return <div style={{ margin: '50vh auto 100vh auto', border: '2px dashed skyblue', maxWidth: 1000 }}>
        <p>Partir de la base de que solamente vamos a identificar como relevantes los productos que ya exportamos competitivamente nos deja un hueco ¿Nos vamos a perder la posibilidad de fomentar la producción de productos verdes complejos solamente porque todavía no los generamos de manera competitiva?
            <br/>Con el objetivo de identificarlos empezamos viendo todos los productos donde Argentina no tiene ventaja competitiva, y los ordenamos por su complejidad futura.</p>
        <div style={{ position: 'sticky', top: "0", marginTop: 150, marginBottom: 150 }}>
            <div ref={ref} className={`chart`}>  </div>
        </div>
        <Scrollama onStepEnter={onStepEnter} debug>
                <Step data={0} key={0}>
                    <div
                        style={{
                            margin: '0 auto',
                            // border: '1px solid gray',
                            opacity: step === 0 ? 1 : 0.2,
                            height: 70,
                            width: 600,
                            zIndex: 200,
                            position: "relative",
                            // background: "white",
                            padding: "16px 24px"
                        }}
                    >
                    </div>
                </Step>
            <Step data={1} key={1}>
                <div
                    style={{
                        margin: '0 auto 100vh auto',
                        border: '1px solid gray',
                        opacity: step === 1 ? 1 : 0.2,
                        width: 600,
                        zIndex: 200,
                        position: "relative",
                        background: "white",
                        padding: "16px 24px"
                    }}
                >
                    De esos productos seleccionamos 25% que tiene VCR mas cercano a 1.
                </div>
            </Step><Step data={2} key={2}>
                <div
                    style={{
                        margin: '0 auto 100vh auto',
                        border: '1px solid gray',
                        opacity: step === 2 ? 1 : 0.2,
                        width: 600,
                        position: "relative",
                        background: "white",
                        padding: "16px 24px"
                    }}
                >
                    De ese subconjunto dejamos aquellos que tuvieran una Perspectiva de Complejidad Futura (COG) mayor.
                </div>
            </Step>
        </Scrollama>
        <p style={{ marginTop: 120 }}>Este segundo criterio no se enfoca primariamente en ponderar los productos de acuerdo a su cercanía en el Espacio de Productos, sino que se orienta primero por identificar y entender la capacidad productiva presente (mirando el VCR) e identificar cuánto aportaría su desarrollo competitivo a una mayor perspectiva de complejidad futura (evaluando la Perspectiva de Complejidad Futura).</p>
    </div>
};

export default ScatterPlot;
