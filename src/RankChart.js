import React from "react";
import * as d3 from "d3";
import './RankChart.css';

const RankChart = ({ data }) => {

    var margin = {top: 100, right: 200, bottom: 50, left: 125, tikX: 15, tikY: 20};
    const width = 960,
        height=800;

    const [isMobile, setMobile] = React.useState(false)

    const [rankData, setRankData] = React.useState([])

    const ref = React.useRef(null);

    const handleResize = () => {
        setMobile(window.innerWidth < 960);
    }

    const theme = {
        default: "#000000",
        argentina: "#7BB5C4",
        china: "#C47B7B",
        indonesia: "#FF9750",
        japon: "#C47BC1",
        uganda: "#B5C47B",
        usa: "#7B98C4"
    }

    React.useEffect(() => {
        handleResize();

        window.addEventListener("resize", handleResize);
        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    const drawEverything = React.useCallback(() => {
        console.log(isMobile)
        if (rankData.length >= 132 && ref.current) {

            d3.select(ref.current).select("svg").remove();

            const svg = d3
                .select(ref.current)
                .append("svg")
                .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            // Use indexOf to fade in one by one
            var highlight = ["USA", "China", "Japan", "Uganda", "Argentina", "Indonesia"];

            svg.append("defs").append("clipPath")
                .attr("id", "clip")
                .append("rect")
                .attr("width", width)
                .attr("height", height + 4);

            var x = d3.scaleLinear()
                .range([0, width]);

            var y = d3.scaleLinear()
                .range([0, height]);

            var line = d3.line()
                // .curve(d3.curveBasis)
                .x(d => x(d.year)).y(d => y(d.rank))

            var xTickNo = rankData[0].ranks.length;
            x.domain(d3.extent(rankData[0].ranks, d => d.year));

            // Ranks
            var ranks = 100;
            y.domain([1, 133]);

            var axisMargin = 0;

            var xAxis = d3.axisTop(x)
                .tickFormat(d3.format("d"))
                .tickValues([2000, 2005, 2010, 2015, 2019])
                .tickSize(margin.tikX);

            var yAxis = d3.axisLeft(y)
                .tickValues([1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130])
                .tickSize(margin.tikY);

            var xGroup = svg.append("g");
            var xAxisElem = xGroup.append("g")
                .attr("transform", "translate(" + [0, -axisMargin] + ")")
                .attr("class", "x-axis")
                .call(xAxis);

            xGroup.append("g").selectAll("line")
                .data(x.ticks(xTickNo))
                .enter().append("line")
                .attr("class", "grid-line")
                .attr("y1", 0)
                .attr("y2", height)
                .attr("x1", d => x(d))
                .attr("x2", d => x(d));

            var yGroup = svg.append("g");
            var yAxisElem = yGroup.append("g")
                .attr("transform", "translate(" + [-axisMargin, 0] + ")")
                .attr("class", "y-axis")
                .call(yAxis);

            yAxisElem.append("text")
                .attr("class", "y-label")
                .attr("text-anchor", "middle")
                .attr("transform", "rotate(-90) translate(" + [-height / 2, -margin.left / 3] + ")")
                .text("Intra-University Ranking");

            yGroup.append("g").selectAll("line")
                .data(y.ticks(ranks/10))
                .enter().append("line")
                .attr("class", "grid-line")
                .attr("x1", 0)
                .attr("x2", width)
                .attr("y1", d => y(d))
                .attr("y2", d => y(d));

            const getClasses = (d, i) => {
                const isHighlighted = highlight.includes(d.country) ? " highlight" : "";
                return "rank-line-g rank-line-g_" + i + isHighlighted;
            }

            var d3Gs = svg.append("g")
                .selectAll("path")
                .data(rankData)
                .enter().append("g")
                .attr("class", (d, i) => getClasses(d, i))

            const d3Lines = d3Gs.append("path")
                .attr("class", d => "rank-line line_" + d.index )
                .attr("d", function(d) {
                    d.line = this;
                    return line(d.ranks)
                })
                .attr("clip-path", "url(#clip)")
                .attr("fill", "none")
                .style("stroke", d => d.color)
                .transition()
                .duration(500)
                .delay(d => (highlight.indexOf(d.country) + 1) * 500)

            const d3Points = d3Gs.selectAll("g").data((data, index) => { return data.ranks.map((punto) => {return {index: index, country: data.country, color: data.color, ...punto}})})
                .enter().append("circle").attr("class", d => "rank-point point_" + d.index)
                .attr("cx", function(d) { return x(d.year) } )
                .attr("cy", function(d) { return y(d.rank) } )
                .attr("r", 2)
                .attr("stroke", d => d.color)
                .attr("fill", d => d.color)

            const d3Labels = d3Gs.append("text")
                .attr("class", "rank-label")
                .attr("x", d => x(d.ranks[d.ranks.length - 1].year))
                .attr("y", d => y(d.ranks[d.ranks.length - 1].rank))
                .attr("dx", 20)
                .attr("dy", 2)
                .text(d => d.ranks[d.ranks.length - 1].rank + ". " + d.country)
                .attr("fill", d => d.color)

            const pointsData = rankData.map((data, index) => {
                return data.ranks.map((punto) => {return {index: index, country: data.country, color: data.color, ...punto}} )
            });

            const voronoi = d3.Delaunay
                .from(pointsData.flat(1), function(d) { return x(d.year) }, function(d) { return y(d.rank) })
                .voronoi([0, 0, width, height]);

            // console.log(pointsData.flat(1))

            svg.append("g")
                .attr("class", "voronoiWrapper")
                .selectAll("path")
                .data(pointsData.flat(1))
                .join("path")
                .attr("opacity", 0.5)
                // .attr("stroke", "#ff1493") // Hide overlay
                .attr("fill", "none")
                .style("pointer-events","visible")
                .attr("d", (d,i) => voronoi.renderCell(i))
                .on("mouseover", (d,i) => mouseover(i))
                .on("mouseout", (d,i) => mouseout(i))
                .on(isMobile ? "touchstart" : "click", (d,i) => click(i))

            function isHovered(index){
                return d3.select(".rank-line-g_" + index).attr("class").includes("hovered")
            }

            function mouseover(d){
                d3.select(".rank-line-g_" + d.index).attr("class", getClasses(d, d.index) + " hovered");
            }

            function mouseout(d){
                d3.select(".rank-line-g_" + d.index).attr("class", getClasses(d, d.index));
            }

            function click(d){
                const hover = isHovered(d.index) ? "" : " hovered"
                d3.select(".rank-line-g_" + d.index).attr("class", getClasses(d, d.index) + hover);
            }

        }
    }, [rankData, isMobile])

    React.useEffect(() => {
        d3.csv("./paises.csv", function(data) {
            var rank = [];
            for (var [key, value] of Object.entries(data)) {
                if (parseInt(key) && parseInt(value) > 0) rank.push({year: parseInt(key), rank: parseInt(value)})
            }

            const obj = {
                country: data.pais,
                ranks: rank,
                color: theme[data.color]
            }

            return obj;
        }).then((objList) => {
            setRankData(objList);
        })
    },[])

    // const drawEverything = () => {
    //     console.log(isMobile)
    //     if (rankData.length >= 132 && ref.current) {
    //
    //         d3.select(ref.current).select("svg").remove();
    //
    //         const svg = d3
    //             .select(ref.current)
    //             .append("svg")
    //             .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`)
    //             .append("g")
    //             .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    //
    //         // Use indexOf to fade in one by one
    //         var highlight = ["USA", "China", "Japan", "Uganda", "Argentina", "Indonesia"];
    //
    //         svg.append("defs").append("clipPath")
    //             .attr("id", "clip")
    //             .append("rect")
    //             .attr("width", width)
    //             .attr("height", height + 4);
    //
    //         var x = d3.scaleLinear()
    //             .range([0, width]);
    //
    //         var y = d3.scaleLinear()
    //             .range([0, height]);
    //
    //         var line = d3.line()
    //             // .curve(d3.curveBasis)
    //             .x(d => x(d.year)).y(d => y(d.rank))
    //
    //         var xTickNo = rankData[0].ranks.length;
    //         x.domain(d3.extent(rankData[0].ranks, d => d.year));
    //
    //         // Ranks
    //         var ranks = 100;
    //         y.domain([1, 133]);
    //
    //         var axisMargin = 0;
    //
    //         var xAxis = d3.axisTop(x)
    //             .tickFormat(d3.format("d"))
    //             .tickValues([2000, 2005, 2010, 2015, 2019])
    //             .tickSize(margin.tikX);
    //
    //         var yAxis = d3.axisLeft(y)
    //             .tickValues([1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130])
    //             .tickSize(margin.tikY);
    //
    //         var xGroup = svg.append("g");
    //         var xAxisElem = xGroup.append("g")
    //             .attr("transform", "translate(" + [0, -axisMargin] + ")")
    //             .attr("class", "x-axis")
    //             .call(xAxis);
    //
    //         xGroup.append("g").selectAll("line")
    //             .data(x.ticks(xTickNo))
    //             .enter().append("line")
    //             .attr("class", "grid-line")
    //             .attr("y1", 0)
    //             .attr("y2", height)
    //             .attr("x1", d => x(d))
    //             .attr("x2", d => x(d));
    //
    //         var yGroup = svg.append("g");
    //         var yAxisElem = yGroup.append("g")
    //             .attr("transform", "translate(" + [-axisMargin, 0] + ")")
    //             .attr("class", "y-axis")
    //             .call(yAxis);
    //
    //         yAxisElem.append("text")
    //             .attr("class", "y-label")
    //             .attr("text-anchor", "middle")
    //             .attr("transform", "rotate(-90) translate(" + [-height / 2, -margin.left / 3] + ")")
    //             .text("Intra-University Ranking");
    //
    //         yGroup.append("g").selectAll("line")
    //             .data(y.ticks(ranks/10))
    //             .enter().append("line")
    //             .attr("class", "grid-line")
    //             .attr("x1", 0)
    //             .attr("x2", width)
    //             .attr("y1", d => y(d))
    //             .attr("y2", d => y(d));
    //
    //         const getClasses = (d, i) => {
    //             const isHighlighted = highlight.includes(d.country) ? " highlight" : "";
    //             return "rank-line-g rank-line-g_" + i + isHighlighted;
    //         }
    //
    //         var d3Gs = svg.append("g")
    //             .selectAll("path")
    //             .data(rankData)
    //             .enter().append("g")
    //             .attr("class", (d, i) => getClasses(d, i))
    //
    //         const d3Lines = d3Gs.append("path")
    //             .attr("class", d => "rank-line line_" + d.index )
    //             .attr("d", function(d) {
    //                 d.line = this;
    //                 return line(d.ranks)
    //             })
    //             .attr("clip-path", "url(#clip)")
    //             .attr("fill", "none")
    //             .style("stroke", d => d.color)
    //             .transition()
    //             .duration(500)
    //             .delay(d => (highlight.indexOf(d.country) + 1) * 500)
    //
    //         const d3Points = d3Gs.selectAll("g").data((data, index) => { return data.ranks.map((punto) => {return {index: index, country: data.country, color: data.color, ...punto}})})
    //             .enter().append("circle").attr("class", d => "rank-point point_" + d.index)
    //             .attr("cx", function(d) { return x(d.year) } )
    //             .attr("cy", function(d) { return y(d.rank) } )
    //             .attr("r", 2)
    //             .attr("stroke", d => d.color)
    //             .attr("fill", d => d.color)
    //
    //         const d3Labels = d3Gs.append("text")
    //             .attr("class", "rank-label")
    //             .attr("x", d => x(d.ranks[d.ranks.length - 1].year))
    //             .attr("y", d => y(d.ranks[d.ranks.length - 1].rank))
    //             .attr("dx", 20)
    //             .attr("dy", 2)
    //             .text(d => d.ranks[d.ranks.length - 1].rank + ". " + d.country)
    //             .attr("fill", d => d.color)
    //
    //         const pointsData = rankData.map((data, index) => {
    //             return data.ranks.map((punto) => {return {index: index, country: data.country, color: data.color, ...punto}} )
    //         });
    //
    //         const voronoi = d3.Delaunay
    //             .from(pointsData.flat(1), function(d) { return x(d.year) }, function(d) { return y(d.rank) })
    //             .voronoi([0, 0, width, height]);
    //
    //         // console.log(pointsData.flat(1))
    //
    //         svg.append("g")
    //             .attr("class", "voronoiWrapper")
    //             .selectAll("path")
    //             .data(pointsData.flat(1))
    //             .join("path")
    //             .attr("opacity", 0.5)
    //             // .attr("stroke", "#ff1493") // Hide overlay
    //             .attr("fill", "none")
    //             .style("pointer-events","visible")
    //             .attr("d", (d,i) => voronoi.renderCell(i))
    //             .on("mouseover", (d,i) => mouseover(i))
    //             .on("mouseout", (d,i) => mouseout(i))
    //             .on(isMobile ? "touchstart" : "click", (d,i) => click(i))
    //
    //         function isHovered(index){
    //             return d3.select(".rank-line-g_" + index).attr("class").includes("hovered")
    //         }
    //
    //         function mouseover(d){
    //             d3.select(".rank-line-g_" + d.index).attr("class", getClasses(d, d.index) + " hovered");
    //         }
    //
    //         function mouseout(d){
    //             d3.select(".rank-line-g_" + d.index).attr("class", getClasses(d, d.index));
    //         }
    //
    //         function click(d){
    //             const hover = isHovered(d.index) ? "" : " hovered"
    //             d3.select(".rank-line-g_" + d.index).attr("class", getClasses(d, d.index) + hover);
    //         }
    //
    //     }
    // }
    //
    React.useEffect(() => {
        drawEverything();
    }, [drawEverything])



    return ( <div ref={ref} className={`chart`}>  </div> );
};

export default RankChart;
