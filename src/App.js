import logo from './logo.svg';
import './App.css';
import RankChart from "./RankChart";
import ScatterPlot from "./ScatterPlot";

function App() {
  return (
    <div className="App">
      <ScatterPlot/>
    </div>
  );
}

export default App;
